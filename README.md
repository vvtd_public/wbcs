# SEGMENTATION AND CLASSIFICATION WHITE BLOOD CELLS
---

## Dataset
[Origin data](https://data.mendeley.com/datasets/snkd93bnjr)

[Processed data](https://drive.google.com/drive/folders/1ducYT8yKQe7D2Hq2_vOGWkwy5Nl5ZiJy?usp=sharing)

## Requirements
```
python==3.8
Django==3.1.5
Pillow==8.1.0
opencv-python--headless
numpy==1.19.2
https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow_cpu-2.5.0-cp38-cp38-manylinux2010_x86_64.whl
```

## Preprocessing
You can preprocess the data with functions in file [here](wbcs/services/preprocessing.py)

## Models
You can retrain the model yourself with AlexNet, VGG16, GoogLeNet, ResNet50 models with the files [here](wbcs/models)

## Run web app
### In Window

Step 1: Setup virtual environment python

Step 1.1: Install virtualenv
```commandline
pip install virtualenv
```
Step 1.2: Check version virtualenv
```commandline
virtualenv --version
```
Step 1.3: Create environment venv
```commandline
virtualenv venv
```
Step 1.4: Active virtual environment
```commandline
env/Scripts/activate
```
Step 2: Install library
```commandline
pip install -r requirements.txt
```
Step 3: Run server Django
```commandline
python manage.py runserver
```
### In Linux
Step 1: Setup virtual environment python

Step 1.1: Install virtualenv
```commandline
pip3 install virtualenv
```
Step 1.2: Check version virtualenv
```commandline
virtualenv --version
```
Step 1.3: Create environment venv
```commandline
virtualenv venv
```
Step 1.4: Active virtual environment
```commandline
env/bin/activate
```
Step 2: Install library
```commandline
pip3 install -r requirements.txt
```
Step 3: Run server Django
```commandline
python3 manage.py runserver
```
