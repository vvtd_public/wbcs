import base64
import numpy as np
from PIL import Image
from io import BytesIO

from keras.models import load_model

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from wbcs.services.segmentation import segmentation
from wbcs.services.classification import classification
from wbcs.services.mutation import convert_images_to_base64

model = load_model('wbcs/weights/GoogLeNet.h5')


@csrf_exempt
def homepage_view(request):
    if request.method == 'POST' and request.FILES.get('submit_file') is not None:
        file = request.FILES['submit_file']
        # Convert image origin to base64
        s = file.read()
        base64_bytes_origin = base64.b64encode(s)
        base64_string_origin = "data:image/png;base64," + base64_bytes_origin.decode("ascii")

        image = np.array(Image.open(BytesIO(s)))
        img_bgr = np.dstack((image[:, :, 0], image[:, :, 1], image[:, :, 2])).astype(np.uint8)

        # Segmentation image
        list_sub_img = segmentation(img_bgr)

        # Predict images
        labels = classification(model, list_sub_img, 'GoogLeNet')

        # Convert image to base64
        list_sub_img_base64 = convert_images_to_base64(list_sub_img)

        list_sub_img_and_labels = zip(list_sub_img_base64, labels)

        return render(request, 'homepage.html', {'base64_string_origin': base64_string_origin,
                                                 'list_sub_img_and_labels': list_sub_img_and_labels})
    return render(request, 'homepage.html')
