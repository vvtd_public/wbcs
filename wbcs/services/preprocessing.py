import os
import cv2
import numpy as np


def convert_image_rbg_to_cmyk(image):
    img_bgr = image.astype(float) / 255.
    with np.errstate(invalid='ignore', divide='ignore'):
        k = 1 - np.max(img_bgr, axis=2)
        c = (1 - img_bgr[..., 2] - k) / (1 - k)
        m = (1 - img_bgr[..., 1] - k) / (1 - k)
        y = (1 - img_bgr[..., 0] - k) / (1 - k)

    cmyk = (np.dstack((c, m, y, k)) * 255).astype(np.uint8)
    return cmyk


def otsu_thresholding(image):
    count_pixel = [0] * 256
    w_image, h_image = image.shape
    for i in range(w_image):
        for j in range(h_image):
            count_pixel[int(image[i, j])] += 1
    total_pixel = sum(count_pixel)
    thresh = 0
    max_between_class_variance = -999999
    for i in range(255):
        total_b = sum(count_pixel[0:i])
        total_f = sum(count_pixel[i:256])
        w_b = total_b / total_pixel
        w_f = total_f / total_pixel
        numerator_b = 0
        numerator_f = 0
        for j in range(i):
            numerator_b += j * count_pixel[j]
        for j in range(i, 256):
            numerator_f += j * count_pixel[j]
        u_b = 0
        u_f = 0
        if total_b > 0:
            u_b = numerator_b / total_b
        if total_f > 0:
            u_f = numerator_f / total_f
        between_class_variance = w_b * w_f * (u_b - u_f) * (u_b - u_f)
        if between_class_variance > max_between_class_variance:
            max_between_class_variance = between_class_variance
            thresh = i
    return thresh


def reverse_image_binary_rbg(image):
    w_image, h_image = image.shape
    for i in range(w_image):
        for j in range(h_image):
            if image[i, j] == 255:
                image[i, j] = 0
            else:
                image[i, j] = 255
    return image


def other():
    root_link = "lymphocyte/"
    for root, dirs, files in os.walk(root_link):
        for filename in files:
            img_bgr = cv2.imread(root_link + filename)

            img_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)

            img_s = img_hsv[:, :, 1]

            thresh = 83
            img_bw = cv2.threshold(img_s, thresh, 255, cv2.THRESH_BINARY)[1]

            nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img_bw, connectivity=8)
            sizes = stats[1:, -1]
            nb_components = nb_components - 1
            min_size = 3500

            img_remove_obj_small = np.zeros(output.shape)
            for i in range(0, nb_components):
                if sizes[i] >= min_size:
                    img_remove_obj_small[output == i + 1] = 1

            img_dilate = cv2.dilate(img_remove_obj_small, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11)),
                                    iterations=5)
            img_erode = cv2.erode(img_dilate, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11)), iterations=5)

            img_erode = np.uint8(img_erode)
            contours, hierarchy = cv2.findContours(img_erode, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            if contours:
                x, y, w_sub, h_sub = cv2.boundingRect(contours[0])
                cropped_contour = img_bgr[y:y + h_sub, x:x + w_sub]

                save_url = root_link + filename
                save_url = save_url.replace('lymphocyte', 'lymphocyte_crop')
                cv2.imwrite(save_url, cropped_contour)


def monocyte():
    root_link = "monocyte/"
    for root, dirs, files in os.walk(root_link):
        for filename in files:
            img_bgr = cv2.imread(root_link + filename)

            img_cmyk = convert_image_rbg_to_cmyk(img_bgr)
            img_c, img_m, img_y, img_k = cv2.split(img_cmyk)

            otsu_thresh = otsu_thresholding(img_y)

            type_process = 'y'
            img_process = img_y
            if otsu_thresh < 10:
                otsu_thresh = otsu_thresholding(img_c)
                img_process = img_c
                type_process = 'c'

            img_bw = cv2.threshold(img_process, otsu_thresh, 255, cv2.THRESH_BINARY)[1]
            if type_process == 'y':
                img_bw = reverse_image_binary_rbg(img_bw)

            nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img_bw, connectivity=8)
            sizes = stats[1:, -1]
            nb_components = nb_components - 1
            min_size = 3500

            img_remove_obj_small = np.zeros(output.shape)
            for i in range(0, nb_components):
                if sizes[i] >= min_size:
                    img_remove_obj_small[output == i + 1] = 1

            img_dilate = cv2.dilate(img_remove_obj_small, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11)),
                                    iterations=5)
            img_erode = cv2.erode(img_dilate, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11)), iterations=5)

            img_erode = np.uint8(img_erode)
            contours, hierarchy = cv2.findContours(img_erode, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            if contours:
                x, y, w_sub, h_sub = cv2.boundingRect(contours[0])
                cropped_contour = img_bgr[y:y + h_sub, x:x + w_sub]

                save_url = root_link + filename
                save_url = save_url.replace('monocyte', 'monocyte_crop')
                cv2.imwrite(save_url, cropped_contour)


def neutrophil():
    root_link = "neutrophil/"
    for root, dirs, files in os.walk(root_link):
        for filename in files:
            img_bgr = cv2.imread(root_link + filename)
            img_lab = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2Lab)

            thresh_otsu = otsu_thresholding(img_lab[:, :, 2])
            img_bw = cv2.threshold(img_lab[:, :, 2], thresh_otsu, 255, cv2.THRESH_BINARY)[1]
            img_bw = reverse_image_binary_rbg(img_bw)

            img_dilate = cv2.dilate(img_bw, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7)),
                                    iterations=5)
            img_erode = cv2.erode(img_dilate, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)), iterations=5)

            nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img_erode, connectivity=8)
            sizes = stats[1:, -1]
            nb_components = nb_components - 1
            min_size = 3500

            img_remove_obj_small = np.zeros(output.shape)
            for i in range(0, nb_components):
                if sizes[i] >= min_size:
                    img_remove_obj_small[output == i + 1] = 1

            img_remove_obj_small = np.uint8(img_remove_obj_small)
            contours, hierarchy = cv2.findContours(img_remove_obj_small, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            if contours:
                x, y, w_sub, h_sub = cv2.boundingRect(contours[0])
                cropped_contour = img_bgr[y:y + h_sub, x:x + w_sub]

                save_url = root_link + filename
                save_url = save_url.replace('neutrophil', 'neutrophil_crop')
                cv2.imwrite(save_url, cropped_contour)
