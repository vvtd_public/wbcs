import cv2
import base64


def convert_images_to_base64(list_sub_img: list) -> list:
    """
    Input: list image white blood cells
    Output: list image base64
    """
    list_sub_img_base64 = []
    for sub_img in list_sub_img:
        ret, buffer = cv2.imencode('.png', sub_img)
        pic_str = base64.b64encode(buffer)
        pic_str = "data:image/png;base64," + pic_str.decode()
        list_sub_img_base64.append(pic_str)
    return list_sub_img_base64
