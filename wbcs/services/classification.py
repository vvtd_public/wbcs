import numpy as np

from keras.applications.inception_v3 import preprocess_input


def classification(model, list_sub_img: list, type_model: str) -> list:
    """
    Input:
    model: model loaded with file weight
    list_sub_img: list image white blood cells
    type_model: GoogLeNet or other
    Output: list labels of list sub image
    """
    labels = []
    class_names = ['Basophil', 'Eosinophil', 'Lymphocyte', 'Monocyte', 'Neutrophil']
    if type_model == 'GoogLeNet':
        input_preprocessing = preprocess_input(np.asarray(list_sub_img))
        y_predict = model.predict(input_preprocessing)
        for y in y_predict[0]:
            labels.append(class_names[np.argmax(y)])
    else:
        y_predict = model.predict(np.asarray(list_sub_img))
        for y in y_predict:
            labels.append(class_names[np.argmax(y)])
    return labels
