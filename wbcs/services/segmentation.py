import cv2
import numpy as np


def segmentation(img_bgr) -> list:
    """
    Input: image BGR
    Output: list sub image
    """
    # Convert image BGR to HSV
    img_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_RGB2HSV)
    img_h = img_hsv[:, :, 0]

    # Convert gray image to binary image
    thresh = 83
    img_bw = cv2.threshold(img_h, thresh, 255, cv2.THRESH_BINARY)[1]

    # Remove noise
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img_bw, connectivity=8)
    nb_components = nb_components - 1
    img_zeros = np.zeros(output.shape)
    count = 0
    for i in range(0, nb_components):
        img_zeros_tmp = np.zeros(output.shape, dtype=np.uint8)
        img_zeros_tmp[output == i + 1] = 255
        contours, hierarchy = cv2.findContours(img_zeros_tmp, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        area = cv2.contourArea(contours[0])
        hull = cv2.convexHull(contours[0])
        hull_area = cv2.contourArea(hull)
        if hull_area == 0:
            hull_area = 1
        solidity = float(area) / hull_area
        if area > 300 and solidity > 0.37:
            img_zeros[img_zeros_tmp == 255] = 255
            count += 1

    # Split white blood cells with watershed
    img_zeros = np.uint8(img_zeros)
    kernel = np.ones((3, 3), np.uint8)
    sure_bg = cv2.dilate(img_zeros, kernel, iterations=3)
    dist_transform = cv2.distanceTransform(img_zeros, cv2.DIST_L2, 5)
    ret, sure_fg = cv2.threshold(dist_transform, 0.37 * dist_transform.max(), 255, 0)
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)
    rets, markers = cv2.connectedComponents(sure_fg)
    markers = markers + 1
    markers[unknown == 255] = 0
    markers = cv2.watershed(img_bgr, markers)

    # Remove noise and split sub image
    w, h, d = img_bgr.shape
    max_markers = np.max(markers)
    list_sub_img = []
    img_watershed = np.zeros((w, h))
    for i in range(2, max_markers + 1):
        img_tmp = np.zeros((w, h))
        img_tmp[markers == i] = 255
        img_tmp = np.uint8(img_tmp)
        contours, hierarchy = cv2.findContours(img_tmp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        if contours:
            area = cv2.contourArea(contours[0])
            if area > 300:
                x, y, w_sub, h_sub = cv2.boundingRect(contours[0])
                cropped_contour = img_bgr[y:y + h_sub, x:x + w_sub]
                cropped_contour = cv2.resize(cropped_contour, (224, 224))
                list_sub_img.append(cropped_contour)
                img_watershed[markers == i] = 255
    return list_sub_img
