from django.urls import path
from wbcs.views import homepage_view


urlpatterns = [
    path('', homepage_view, name='homepage'),
]
