function updateValue(e) {
    const input = document.querySelector('input');
    input.addEventListener('input', updateValue);
    var ext = input.value.split('.').pop().toLowerCase();
    var arr_ext = ['gif','png','jpg','jpeg'];
    let ele = document.getElementById("error_msg");
    if (arr_ext.indexOf(ext) === -1) {
        ele.innerText = "Tệp không phải hình ảnh";
    }
	else {
        ele.innerText = "";
        let button_outer = document.getElementById("button_outer");
        button_outer.classList.add("file_uploading");
        setTimeout(function(){
            button_outer.classList.add("file_uploaded");
        },3000);
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#img_upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
	};
};

function clickSubmit(e){
    var element = document.getElementById("btn_submit");
    element.classList.add("processing");
    setTimeout(function() {
      element.classList.remove("processing");
      element.classList.add("validate");
      setTimeout(function() {
        element.classList.remove("validate");
      }, 1250);
    }, 2250);
}