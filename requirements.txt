Django==3.1.5
Pillow==8.1.0
opencv-python--headless
numpy==1.19.2
https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow_cpu-2.5.0-cp38-cp38-manylinux2010_x86_64.whl
